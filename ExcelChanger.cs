﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;
using System.Reflection;

namespace ExcelChanger {
    public partial class ExcelChanger : Form
    {
        #region Data
        /// <summary>
        /// The excel application process
        /// </summary>
        Excel.Application application = null;

        /// <summary>
        /// The excel workbook opened via the application
        /// </summary>
        Workbook workbook = null;

        /// <summary>
        /// Path to the chosen excel file
        /// </summary>
        string path = string.Empty;

        /// <summary>
        /// Filename chosen
        /// </summary>
        string filename = string.Empty;

        /// <summary>
        /// An enumerated list of values that are to be manipulated by the program.
        /// If at some point in time, the rows / columns that need to be manipulated
        /// by this program are to be changed, this list as well as the function 
        /// calls associated with the deletion should be modified.
        /// </summary>
        enum DeletedValues
        {
            Address,
            City,
            Step,
            EmpId,
            Campus,
            Sacramento,
        }
        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new ExcelChanger instance. Inside this constructor
        /// instanciates a new Excel.Application() for modification.
        /// </summary>
        public ExcelChanger() {
            InitializeComponent();

            application = new Excel.Application();
        }
        #endregion

        #region Events

        /// <summary>
        /// Browse button click. Initiated when the "btnBrowse" button is pressed.
        /// This function will launch the getPathToFile() process. 
        /// </summary>
        /// <param name="sender">btnBrowse</param>
        /// <param name="e">Empty EventArgs</param>
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            path = getPathToFile();
            tbPath.Text = path;
        }

        /// <summary>
        /// Exit button click. Initiated when the "btnExit" button is pressed.
        /// This function simply exits the program
        /// </summary>
        /// <param name="sender">btnExit</param>
        /// <param name="e">Empty EventArgs</param>
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Delete button click. Initiated when the "btnDelete" button is pressed.
        /// This function will confirm to the user if they wish to modify their
        /// excel file, and proceed to do the modification. Once modifications
        /// are made, the excel file is to be saved and a confirmation is shown
        /// to the client explaining that the modifications were a success.
        /// </summary>
        /// <param name="sender">btnDelete</param>
        /// <param name="e">Empty EventArgs</param>
        private void btnDelete_Click(object sender, EventArgs e) {
            try 
            {
                // if the user wishes to continue and delete the rows, execute the script.
                // otherwise, do nothing.
                if (MessageBox.Show("This will modify your Excel document. Continue?",
                    "Continue", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) ==
                        System.Windows.Forms.DialogResult.OK)
                {
                    workbook = application.Workbooks.Open(
                        path, Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value);

                    //Call the functions to delete the given values from the excel sheet.
                    // In its current iteration, these functions delete the column entries
                    // passed into the functions, and will KEEP the row entries passed into 
                    // the delete rows function.
                    //NOTE:: If the values were to ever change, the following process needs to
                    // be taken.
                    //      1) DeletedValues enum modified with the correct label
                    //      2) Function calls need to be modified to delete the correct value.
                    deleteColEntries(workbook, DeletedValues.Address.ToString());
                    deleteColEntries(workbook, DeletedValues.City.ToString());
                    deleteColEntries(workbook, DeletedValues.Step.ToString());
                    deleteColEntries(workbook, DeletedValues.EmpId.ToString());
                    deleteRowEntries(workbook, DeletedValues.Campus.ToString(), DeletedValues.Sacramento.ToString());

                    //All of our deletions have been complete. Save the workbook
                    workbook.Save();

                    // Let the user know this was a triumph.
                    MessageBox.Show(this, "Excel file successfully modified", "Success", MessageBoxButtons.OK);
                }
            }
            catch
            { 
                MessageBox.Show("Unable to open file", "Error:", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            //close and quit excel
            if (workbook != null) workbook.Close();
            if (application != null) application.Quit();

        }

        #endregion

        #region Row / Column Manipulation 

        /// <summary>
        /// Delete row entries from a given excel workbook. 
        /// Iterates through a given workbook's worksheets looking for a column label to 
        /// scan. Once the column label has been found, iterate through the row cells looking 
        /// for a specific string. In this program, we keep any rows that contain the given
        /// string, and delete any rows that do not contain the specific string.
        /// </summary>
        /// <param name="workbook">Excel workbook to modify</param>
        /// <param name="col">Column label to scan for row to manipulate</param>
        /// <param name="row">Row label to keep</param>
        private void deleteRowEntries(Workbook workbook, string col, string row) {
            // we will be deleting all rows that DO NOT contain sacramento
            row = row.Trim();

            // Get sheet Count and store the number of sheets.
            int numSheets = workbook.Sheets.Count;

            // Iterate through the sheets. They are indexed starting at 1.
            for (int sheetNum = 1; sheetNum < numSheets + 1; sheetNum++) {
                Worksheet sheet = (Worksheet)workbook.Sheets[sheetNum];

                // Take the used range of the sheet. Finally, get an object array of all
                // of the cells in the sheet (their values). You can do things with those
                // values. See notes about compatibility.
                int rows = sheet.UsedRange.Rows.Count;
                int cols = sheet.UsedRange.Columns.Count;

                Range excelRange = sheet.UsedRange;
                object[,] valueArray = (object[,])excelRange.get_Value(
                    XlRangeValueDataType.xlRangeValueDefault);


                // find the col that contains the rows we need to manipulate
                List<int> delete = new List<int>();
                for (int i = 1; i <= cols; ++i)
                {
                    try
                    {
                        if (valueArray == null) continue;
                        if (valueArray[1, i] == null) continue;
                        string val = (string)valueArray[1, i];
                        val = RemoveGapFromString(val);

                        //if val contains "CAMPUS" iterate through the cells to check
                        // for "Sacramento" and delete any non Sacramento entries. 
                        if (val.ToLower().Contains(col.ToLower().Trim()))
                        {
                            // start at 3 since the first two rows are labels
                            for (int k = 3; k <= rows; ++k)
                            {
                                if (valueArray[k, i] == null) continue;
                                string rowVal = (string)valueArray[k, i];
                                rowVal = rowVal.ToLower().Trim();

                                if (!rowVal.Contains(row.ToLower().Trim()))
                                    delete.Add(k);
                            }
                        }
                    }
                    catch
                    {
                        MessageBox.Show(this, "Unable to edit rows.", "Error");
                    }
                }

                // if the col couldn't be found, don't delete anything
                if (delete.Count == 0) continue;

                // delete the col
                delete.Reverse();
                foreach (int i in delete) rowDelete(sheet, GetExcelName(i) + i.ToString());
                
                //once the row has been deleted, save the workbook
                workbook.Save();
            }
        }

        /// <summary>
        /// Delete column entries from a given excel workbook. 
        /// Iterates through a given workbook's worksheets looking for a column label. Once
        /// the column label has been found, it will delete that column from the workbook.
        /// </summary>
        /// <param name="workbook">Excel workbook to modify</param>
        /// <param name="col">Column label to delete</param>
        private void deleteColEntries(Workbook workbook, string col) {
            col = col.Trim();
            // Get sheet Count and store the number of sheets.
            int numSheets = workbook.Sheets.Count;

            // Iterate through the sheets. They are indexed starting at 1.
            for (int sheetNum = 1; sheetNum < numSheets + 1; sheetNum++) {
                Worksheet sheet = (Worksheet)workbook.Sheets[sheetNum];

                // Take the used range of the sheet. Finally, get an object array of all
                // of the cells in the sheet (their values). You can do things with those
                // values. See notes about compatibility.
                int rows = sheet.UsedRange.Rows.Count;
                int cols = sheet.UsedRange.Columns.Count;

                Range excelRange = sheet.UsedRange;
                object[,] valueArray = (object[,])excelRange.get_Value(
                    XlRangeValueDataType.xlRangeValueDefault);


                // find the col that needs to be deleted
                List<int> delete = new List<int>();
                for (int i = 1; i <= cols; ++i)
                {
                    try
                    {
                        if (valueArray == null) continue;
                        if (valueArray[1, i] == null) continue;
                        string val = (string)valueArray[1, i];
                        val = RemoveGapFromString(val);
                        if (val.ToLower().Contains(col.ToLower().Trim()))
                            delete.Add(i);
                    }
                    catch 
                    {
                        MessageBox.Show(this, "Unable to edit columns.", "Error");
                    }
                }

                // if the col couldn't be found, don't delete anything
                if (delete.Count == 0) continue;

                // delete the col
                foreach (int i in delete) columnDelete(sheet, GetExcelName(i) + i.ToString());

                //once the col has been deleted, save the workbook
                workbook.Save();
            }
        }

        /// <summary>
        /// RowDelete performs the act of deleting a row from the Excel worksheet
        /// </summary>
        /// <param name="sheet">Worksheet to delete said row from</param>
        /// <param name="value">Row identifier to delete</param>
        private void rowDelete(Worksheet sheet, string value) {
            Excel.Range range = (Excel.Range)sheet.get_Range(value, Missing.Value);
            range.EntireRow.Delete(Missing.Value);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(range);
        }

        /// <summary>
        /// Column delete performs the act of deleting a column from the Excel worksheet. 
        /// </summary>
        /// <param name="sheet">Worksheet to delete said row from</param>
        /// <param name="value">Column identifier to delete</param>
        private void columnDelete(Worksheet sheet, string value) {
            Excel.Range range = (Excel.Range)sheet.get_Range(value, Missing.Value);
            range.EntireColumn.Delete(Missing.Value);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(range);
        }
        #endregion

        #region Utility Functions

        /// <summary>
        /// Algorithm for converting a row or column number to excel name.
        /// </summary>
        /// <param name="count">Row or Column to get name for</param>
        /// <returns>The name used to identify the row or column in an excel worksheet</returns>
        public string GetExcelName(int count) {
            return new string((char)('A' + (((count - 1) % 26))), ((count - 1) / 26) + 1);
        }

        /// <summary>
        /// Takes a string and removes any blank characters. For example, converts
        /// "Office Space" to "OfficeSpace"
        /// </summary>
        /// <param name="val">String to convert</param>
        /// <returns>String with all gaps removed</returns>
        private string RemoveGapFromString(string val)
        {
            string result = string.Empty;
            string[] vals = val.Split(' ');
            foreach (string s in vals)
                result += s;
            return result;
        }
                
        /// <summary>
        /// Opens a dialog for user to select an excel file from a specified location.
        /// If the file is clicked, return the path of that file, otherwise return an 
        /// empty string.
        /// </summary>
        /// <returns></returns>
        private string getPathToFile() {
            string pathToFile = string.Empty;

            try 
            {
                OpenFileDialog ofd = new OpenFileDialog();
                if (ofd.ShowDialog() == DialogResult.OK) {
                    filename = System.IO.Path.GetFileName(ofd.FileName);
                    path = System.IO.Path.GetDirectoryName(ofd.FileName) + "\\";
                    pathToFile = System.IO.Path.GetFullPath(ofd.FileName);
                }
            }
            catch 
            {
                MessageBox.Show(this,"Error opening file.");
            }

            return pathToFile;
        }
        #endregion

    }
}
